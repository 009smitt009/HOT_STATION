/*
 LiquidCrystal Library - Hobbytronics
 
 Demonstrates the use a 16x2 LCD display.  The LiquidCrystal
 library works with all LCD displays that are compatible with the 
 Hitachi HD44780 driver. There are many of them out there, and you
 can usually tell them by the 16-pin interface.
 
 This sketch prints "Hobbytronics" to the LCD
 and shows the time.
 This sketch is based on the Arduino sample sketch at 
 http://www.arduino.cc/en/Tutorial/LiquidCrystal
 but with modifications  to the LCD contrast to make it 
 adjustable via software
 
  The circuit:
 * LCD RS pin to digital pin 4.
 * LCD Enable pin to digital pin 5.
 * LCD R/W pin to Ground 
 * LCD VO pin (pin 3) to PWM pin 3.
 * LCD D4 pin to digital pin 2.
 * LCD D5 pin to digital pin 12.
 * LCD D6 pin to digital pin 7.
 * LCD D7 pin to digital pin 8.

 */

#define B 3950 // B-коэффициент
#define SERIAL_R 10000 // сопротивление последовательного резистора, 102 кОм
#define THERMISTOR_R 10000 // номинальное сопротивления термистора, 100 кОм
#define NOMINAL_T 25 // номинальная температура (при которой TR = 100 кОм)
const byte tempPin = A3;
#define PIN_ACP A0
#define PIN_TEN 9

// include the library code:
#include <LiquidCrystal.h>

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(4, 5, 2, 12, 7, 8);

void setup() {
  Serial.begin(9600);
  // declare pin 9 to be an output:
  pinMode(3, OUTPUT); 
  pinMode(PIN_TEN, OUTPUT); 
  analogWrite(3, 255);  
   pinMode( tempPin, INPUT ); 
  // set up the LCD's number of columns and rows: 
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("TEMP:");
}

void loop() {
int val=analogRead(PIN_ACP);
  //Serial.println(val);
  lcd.setCursor(0,1);
  lcd.print("VALUE:");
  
  lcd.setCursor(9,1);
  lcd.print("%");
  lcd.setCursor(6, 0);
  lcd.print(Gettemp());
int valx = map(val,0,1022,0,100);

Serial.println(map(val,0,1022,0,255));

  analogWrite(PIN_TEN,map(val,0,1022,0,255));
  if (valx<9){lcd.setCursor(7,1);lcd.print(" ");}
  if ((valx>9)&&(valx<99)){lcd.setCursor(8,1);lcd.print(" ");}
  lcd.setCursor(6,1);
  lcd.print(valx);
}



double Gettemp() {
   int t = 1023-analogRead( tempPin );
    float tr = 1023.0 / t - 1;
    tr = SERIAL_R / tr;
//    Serial.print("R=");
//    Serial.print(tr);
//    Serial.print(", t=");

    float steinhart;
    steinhart = tr / THERMISTOR_R; // (R/Ro)
    steinhart = log(steinhart); // ln(R/Ro)
    steinhart /= B; // 1/B * ln(R/Ro)
    steinhart += 1.0 / (NOMINAL_T + 273.15); // + (1/To)
    steinhart = 1.0 / steinhart; // Invert
    steinhart -= 273.15; 
    //Serial.println(steinhart);
 
    delay(100);
  return (int)steinhart;
}
